$("#btn_send").click(function(){
	//Get params values
	var data = {
		user: $("#input_user").val() || "Guest",
		msg: $("#input_msg").val()
	}
	
	if(!data.msg){
		printError("You can not send an empty message");
		return false;
	}
	
	$.ajax({
		method: "GET",
		url: "comment/new",
		data: data
	})
	.done(function(res){
		try{
			var resp = JSON.parse(res);
			if(resp.code != 200)
				printError(resp.data)
		}
		catch(e){
			printError("Error sending the message");
		}
	})
	.error(function(){
		printError("Error sending the message");
	});
	
	return false;
});

function printError(msg){
	$("#notify_msg").remove();
	$("#btn_send").before('<div id="notify_msg" class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Error: </strong>' + msg + '</div>');
}