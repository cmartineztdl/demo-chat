var phonecatApp = angular.module('chat_demo', []);

phonecatApp.controller('msg_list_controller', function ($scope) {
	getMessages(function(messages){
		$scope.msgs = messages;
		$scope.$apply();
	});
	
	setInterval(function(){
		getMessages(function(messages){
			$scope.msgs = messages;
			$scope.$apply();
		});
	},200);
});

function getMessages(callback){
	$.ajax({
		method: "GET",
		url: "comment/",
	})
	.done(function(res){
		try{
			var resp = JSON.parse(res);
			if(resp.code == 200)
				callback(resp.data)
			else
				callback([])
		}
		catch(e){
			callback([]);
		}
	})
	.error(function(){
		callback([]);
	});
}