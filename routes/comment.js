var express = require('express');
var router = express.Router();

var mongo = require('mongoskin');

//mongo db
var db = mongo.db(process.env.mongo, {native_parser:true});

/* GET users listing. */
router.get('/', function(req, res) {
	db.collection("messages").find().toArray(function(err, messages){
		if(err) return res.send(genRes(400, "Error getting the messages."));
		res.send(genRes(200, messages));
	});
	
});

router.get('/new', function(req, res) {
	var message = {
			user: req.param("user"),
			msg: req.param("msg"),
			date: Date.now()
	}
	
	if(!message.msg && !message.user)
		return res.send(genRes(400,"Message and user are necesary"));
	
	db.collection("messages").insert(message, function(err){
		if(err) return res.send(genRes(400, "Error saving the message"));
		res.send(genRes(200, "OK"))
	});
});

function genRes(code, data){
	var res = {
			code: code,
			data: data
	};
	return JSON.stringify(res);
}

module.exports = router;
